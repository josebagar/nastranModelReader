# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os
import sys

# We don't allow colours in Windows nor when stdout and stdin don't match
# so that when the user is redirecting stdout to a file they don't get
# garbage
nocolours = False
if os.fstat(0) != os.fstat(1):
    nocolours = True

# Colors for terminal output
GREEN = '\033[92m'
RED = '\033[91m'
YELLOW = '\033[93m'
ENDC = '\033[0m'
if nocolours:
    GREEN = ''
    RED = ''
    YELLOW = ''
    ENDC = ''


def log(s):
    """This function will log the given message. It outputs to stdout."""
    s = s.strip()
    sys.stdout.write(GREEN+'NOTE: '+ENDC+'{0}\n'.format(s))


def warning(s):
    """This function will log the given message as a warning. It outputs
    to stdout."""
    s = s.strip()
    sys.stdout.write(YELLOW+'WARNING: '+ENDC+'{0}\n'.format(s))


def error(s):
    """This function will log the given message as an error. It outputs
    to stderr."""
    s = s.strip()
    sys.stdout.write(RED+'ERROR: '+ENDC+'{0}\n'.format(s))
