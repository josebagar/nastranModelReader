# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from parsing.parsing import nas_float


class Dmi(np.matrix):
    """A 2D Matrix that corresponds to a Natran DMI card"""
    # Subclassing np.matrix -or np.ndarray, for the matter- is a bit special
    def __new__(cls, dimension, name, tin, tout):
        dtype = np.float64
        if tin in (3, 4):
            dtype = np.complex128
        obj = np.asarray(np.zeros(dimension), dtype=dtype).view(cls)
        obj.name = name
        obj.tin = tin
        obj.tout = tout
        # Finally, we must return the newly created object:
        return obj

    def __array_finalize__(self, obj):
        if isinstance(obj, np.matrix):
            np.matrix.__array_finalize__(self, obj)

    def __array_wrap__(self, out_arr, context=None):
        return np.ndarray.__array_wrap__(self, out_arr, context)


def DmiParser(parent, notation, fields):
    """Given a Nastran DMI card, parse it and return a Dmi object if it's not already defined.
    If it is, update its values"""
    parent.register_component('matrices')

    # We want this to fail if no necessary params are given
    name = fields[1]
    col = int(fields[2])

    # If col == 0  => Define the Matris
    #    otherwise => Update values
    # We'll just overwrite the matrix (without warning) if the same matrix is
    # defined (col == 0) more than once in the model
    if col == 0:
        form = int(fields[3])
        tin = int(fields[4])
        tout = int(fields[5])
        m = int(fields[7])
        n = int(fields[8])
        parent.matrices[name] = Dmi((m, n), name, tin, tout)
    else:
        if not name in parent.matrices.keys():
            raise ValueError('Matrix {} values defined before its header'.format(name))

        col = int(fields[2])-1
        row = None
        for field in fields[3:]:
            if field != '':
                if not '.' in field:
                    row = int(field)-1
                else:
                    parent.matrices[name][row,col] = nas_float(field)
                    row += 1