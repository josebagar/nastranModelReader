# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

"""Miscellaneous Nastran parsing code goes here"""


def get_fields(line):
    """Read the fields from the given line. Try to detect the format the line was written in"""
    blocks = []
    # Free field format
    if ',' in line:
        blocks = line.split(',')
        if len(blocks) > 9:
            blocks = blocks[0:9]

        return blocks, 'free'

    # Store the different cards
    blocks.insert(0, line[0:8].strip())
    field = 1
    pos = 8

    # Vars for short field notation
    width = 8
    fields = 9

    # Vars for large field notation (first block starts or ends with *)
    notation = 'short'
    if line[0:8].strip().endswith('*') or line.startswith('*'):
        notation = 'large'
        width = 16
        fields = 5

    while pos < 72:
        blocks.insert(field, line[pos:pos+width].strip())
        pos += width
        field += 1
    # Don't care about field 10, really
    # blocks.insert(field, line[pos:pos+8].strip())

    # Remove that '*' from large field notation blocks
    if notation == 'large':
        blocks[0] = blocks[0].replace('*', '')

    return blocks, notation


def value_or_default(fields, n, default):
    "Return a default value in case the nth element of iterable fields is not defined or blank"
    try:
        if fields[n] == '':
            return default
        else:
            return fields[n]
    except IndexError:
        return default


def nas_float(n):
    """Try to handle Nastran's 1.23-1 like notation (where there's no 'E')
    This makes the process of reading the nodes a bit slower, but more resillient"""
    if isinstance(n, str):
        if n.startswith('.'):
            n = '0'+n
        pos_minus = n.strip().find('-', 1)
        pos_plus = n.strip().find('+', 1)
        if pos_minus != -1 and n.find('E-') == -1:
            n = n[:pos_minus] + 'E-' + n[pos_minus+1:]
        if pos_plus != -1 and n.find('E+') == -1:
            n = n[:pos_plus] + 'E+' + n[pos_plus+1:]

    return float(n)
