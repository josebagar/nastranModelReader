# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from elems.point import Point
import parsing.parsing as parsing


class CTria3:
    """A triangular element in 3D space, with an optional id"""

    def __init__(self, elid, pid, A, B, C, theta=None, mcid=None,
                 zoffs=0., tflag=None, t1=1., t2=1., t3=1.,
                 construct=True, parent=None):
        self.A = A
        self.B = B
        self.C = C
        self.elid = elid
        self.pid = pid
        self.theta = theta
        self.mcid = mcid
        self.zoffs = zoffs
        self.tflag = tflag
        self.t1 = t1
        self.t2 = t2
        self.t3 = t3
        self.parent = parent
        self.type = 'ctria3'

        # Extra attributes that will get filled when calling `construct`
        self.G = None
        self.normal = None
        self.area = None
        self.pos = None

        self.constructed = False
        if construct:
            self.construct()

    def construct(self):
        """This is the object's constructor method. It'll be run after the whole model has been
        read.
        This allows us to declare element cards before the grid point cards they're based on and so
        on."""
        if self.constructed:
            return

        self.A = self.parent.nodes[self.A]
        self.B = self.parent.nodes[self.B]
        self.C = self.parent.nodes[self.C]
        self.G = Point((self.A.pos + self.B.pos + self.C.pos) / 3)
        self.normal = np.cross((self.B.pos - self.A.pos), (self.C.pos - self.A.pos))
        self.area = np.linalg.norm(self.normal) / 2
        # Bounding box limits
        self.pos = [min(self.A.pos[0], self.B.pos[0], self.C.pos[0]),
                    min(self.A.pos[1], self.B.pos[1], self.C.pos[1]),
                    min(self.A.pos[2], self.B.pos[2], self.C.pos[2])]
        upperbound = [max(self.A.pos[0], self.B.pos[0], self.C.pos[0]),
                      max(self.A.pos[1], self.B.pos[1], self.C.pos[1]),
                      max(self.A.pos[2], self.B.pos[2], self.C.pos[2])]
        # A kind of position vector for the bounding box in a R2N space
        self.pos.extend(upperbound)
        self.constructed = True

    def __repr__(self):
        if self.constructed:
            return "CTria3 {} with center @ ({})".format(self.elid, ', '.join([str(pos) for pos in
                                                                               self.G.pos]))
        else:
            return "Unconstructed CTria3 {}".format(self.elid)


def CTria3Parser(parent, notation, fields):
    """This function gets an iterable with the parameters in a Nastran CTRIA3 card and
    returns a CQuad4 object"""
    # If needed, create the parent's dictionary
    parent.register_component('elems')

    elid = int(fields[1])
    pid = int(parsing.value_or_default(fields, 2, elid))
    p1 = int(fields[3])
    p2 = int(fields[4])
    p3 = int(fields[5])
    # Field 6 can be either an integer or a float, and they mean different things
    theta = None
    mcid = None
    try:
        if fields[6] == '':
            theta = 0.
        elif '.' in fields[6]:
            theta = parsing.nas_float(fields[6])
        else:
            mcid = int(fields[6])
    except IndexError:
        theta = 0.
    zoffs = parsing.nas_float(parsing.value_or_default(fields, 7, 0.))
    tflag = parsing.value_or_default(fields, 8, None)
    t1 = parsing.nas_float(parsing.value_or_default(fields, 9, 1.))
    t2 = parsing.nas_float(parsing.value_or_default(fields, 10, 1.))
    t3 = parsing.nas_float(parsing.value_or_default(fields, 11, 1.))
    parent.elems[elid] = CTria3(elid, pid, p1, p2, p3,
                                theta, mcid, zoffs, tflag,
                                t1, t2, t3,
                                False, parent)
