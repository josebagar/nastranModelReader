# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from elems.point import Point
import parsing.parsing as parsing


class CQuad4():
    """A quadrangular element in 3D space, with an optional id.
    The four points are assumed to be given in an order which follows the right hand rule.
    The element is assumed to be a parallelogram"""
    def __init__(self, elid, pid, A, B, C, D, theta=None, mcid=None,
                  zoffs=0., tflag=None, t1=1., t2=1., t3=1., t4=1.,
                  notation='short', construct=True, parent=None):
        self.A = A
        self.B = B
        self.C = C
        self.D = D
        self.G = None
        self.elid = elid
        self.pid = pid
        self.theta = theta
        self.mcid = mcid
        self.zoffs = zoffs
        self.tflag = tflag
        self.t1 = t1
        self.t2 = t2
        self.t3 = t3
        self.t4 = t4
        self.parent = parent
        self.constructed = False
        if construct:
            self.construct()

    def construct(self):
        """This is the object's constructor method. It'll be run after the whole model has been
        read.
        This allows us to declare element cards before the grid point cards they're based on and so
        on."""
        if self.constructed:
            return

        self.A = self.parent.nodes[self.A]
        self.B = self.parent.nodes[self.B]
        self.C = self.parent.nodes[self.C]
        self.D = self.parent.nodes[self.D]
        self.G = Point( (self.A.pos+self.B.pos+self.C.pos+self.D.pos)/4 )
        self.normal = np.cross( (self.B.pos-self.A.pos), (self.D.pos-self.A.pos) )
        # Parallelogram area
        self.area   = np.linalg.norm( self.normal )
        self.normal = self.normal / self.area
        self.v1 = (self.B.pos-self.A.pos) / np.linalg.norm(self.B.pos-self.A.pos)
        self.v2 = (self.D.pos-self.A.pos) / np.linalg.norm(self.D.pos-self.A.pos)
        self.type = 'cquad4'
        # Bounding box limits
        self.pos   = [ min(self.A.pos[0], self.B.pos[0], self.C.pos[0], self.D.pos[0]),
                       min(self.A.pos[1], self.B.pos[1], self.C.pos[1], self.D.pos[1]),
                       min(self.A.pos[2], self.B.pos[2], self.C.pos[2], self.D.pos[2]) ]
        upperbound = [ max(self.A.pos[0], self.B.pos[0], self.C.pos[0], self.D.pos[0]),
                       max(self.A.pos[1], self.B.pos[1], self.C.pos[1], self.D.pos[1]),
                       max(self.A.pos[2], self.B.pos[2], self.C.pos[2], self.D.pos[2]) ]
        # A kind of position vector for the bounding box in a R2N space
        self.pos.extend(upperbound)
        self.constructed = True

    def __repr__(self):
        if self.constructed:
            return "CQuad4 {} with center @ ({})".format(self.elid, ', '.join([str(pos) for pos in
                                                                               self.G.pos]))
        else:
            return "Unconstructed CQuad4 {}".format(self.elid)


def CQuad4Parser(parent, notation, fields):
    """This function gets an iterable with the parameters in a Nastran CQUAD4 card and
    returns a CQuad4 object"""
    # If needed, create the parent's dictionary
    parent.register_component('elems')

    elid = int(fields[1])
    pid = int(parsing.value_or_default(fields, 2, elid))
    p1 = int(fields[3])
    p2 = int(fields[4])
    p3 = int(fields[5])
    p4 = int(fields[6])
    # Field 7 can be either an integer or a float, and they mean different things
    theta = None
    mcid = None
    try:
        if fields[6] == '':
            theta = 0.
        elif '.' in fields[6]:
            theta = parsing.nas_float(fields[6])
        else:
            mcid = int(fields[6])
    except IndexError:
        theta = 0.
    zoffs = parsing.nas_float(parsing.value_or_default(fields, 8, 0.))
    tflag = parsing.value_or_default(fields, 9, None)
    t1 = parsing.nas_float(parsing.value_or_default(fields, 10, 1.))
    t2 = parsing.nas_float(parsing.value_or_default(fields, 11, 1.))
    t3 = parsing.nas_float(parsing.value_or_default(fields, 12, 1.))
    t4 = parsing.nas_float(parsing.value_or_default(fields, 13, 1.))
    parent.elems[elid] = CQuad4(elid, pid, p1, p2, p3, p4,
                                theta, mcid, zoffs, tflag,
                                t1, t2, t3, t4,
                                notation, False, parent)