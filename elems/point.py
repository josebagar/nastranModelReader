# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
import parsing.parsing as parsing


class Point():
    """A Point entity in space. pos is supposed to be an iterable containing the N-dimensional
    position of the point.
    The coordinates given to this point should always be expressed in the same cartesian coordinate
    system"""
    def __init__(self, pos):
        self.pos = np.array( pos )

    def distance(self, otherPoint):
        """Return the Euclidean distance between this point and another given one"""
        return np.linalg.norm(otherPoint.pos - self.pos)


class Grid(Point):
    """A Grid entity, with all the properties given to a Nastran GRID card"""
    def __init__(self, pos, gid=None, cp=None, cd=None, ps=None, seid=None, notation=None,
                 construct=False, parent=None):
        self.__pos__ = np.array(pos)
        self.gid = gid
        self.cp = cp
        self.cd = cd
        self.ps = ps
        self.seid = seid
        self.notation = notation
        self.parent = parent
        # If the node's position is expressed in the basic coordinate system,
        # construct it. Otherwise, mark it for construction
        if cp == 0:
            super().__init__(self.__pos__)
            del self.__pos__
            self.constructed = True
        else:
            self.constructed = False
            if construct:
                self.construct()

    def construct(self):
        """This is the object's constructor method. It'll be run after the whole model has been
        read.
        This allows us to declare element cards before the grid point cards they're based on and so
        on."""
        if self.constructed:
            return

        # If the coordinates are expressed in a system other than the basic, construct
        # that CS and then express convert them
        self.parent.coords[self.cp].construct()

        # Get the position of the origin of the CS in the basic CS
        pos = self.parent.coords[self.cp].toBasic(self.__pos__ )

        super().__init__(pos)

        # If needed, store arbitrary initial values for the maximum and minimum positions
        if self.parent.min is None:
            self.parent.min = Point( [x_i for x_i in self.parent.nodes[self.gid].pos] )
        if self.parent.max is None:
            self.parent.max = Point( [x_i for x_i in self.parent.nodes[self.gid].pos] )

        # Store the minimum and maximum values of the positions, if needed
        # I'm guessing this can get slow...
        for i in range(len(self.parent.nodes[self.gid].pos)):
            if self.parent.min.pos[i] > self.parent.nodes[self.gid].pos[i]:
                self.parent.min.pos[i] = self.parent.nodes[self.gid].pos[i]

            if self.parent.max.pos[i] < self.parent.nodes[self.gid].pos[i]:
                self.parent.max.pos[i] = self.parent.nodes[self.gid].pos[i]

        del self.__pos__
        self.constructed = True

    def __repr__(self):
        if self.gid is not None:
            return '{}: ({})'.format(self.gid, ', '.join([str(pos) for pos in self.pos]))
        else:
            return '({})'.format(', '.join([str(pos) for pos in self.pos]))


def GridParser(parent, notation, fields):
    """This function gets an iterable with the parameters in a Nastran GRID card and
    returns a Point object"""
    # If needed, create the parent's dictionary
    parent.register_component('nodes')

    # We want the parsing to fail if no node ID is given
    gid = int(fields[1])
    cp = int(parsing.value_or_default(fields, 2, 0))
    x1 = parsing.nas_float(parsing.value_or_default(fields, 3, 0.))
    x2 = parsing.nas_float(parsing.value_or_default(fields, 4, 0.))
    x3 = parsing.nas_float(parsing.value_or_default(fields, 5, 0.))
    cd = int(parsing.value_or_default(fields, 6, 0))
    ps = parsing.value_or_default(fields, 7, '')
    seid = int(parsing.value_or_default(fields, 8, 0))
    parent.nodes[gid] = Grid((x1, x2, x3), gid, cp, cd, ps, seid, notation=notation,
                             construct=False, parent=parent)
