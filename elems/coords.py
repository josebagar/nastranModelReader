# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import numpy as np
from elems.point import Point
import parsing.parsing as parsing


class CoordinateSystem():
    """A cartesian coordinate system. It needs an origin and a rotation matrix, both of which
    should be expressed in the basic coordinate system."""
    def __init__(self, origin, Q):
        self.origin = Point(origin)
        self.Q = Q
        self.N = len(origin)

    def fromBasic(self, vector):
        """Given a vector in the basic coordinate system, return its expression in this system.
        Any iterable containing floats of the same dimension as this CS will do for vector"""
        return vector

    def toBasic(self, vector):
        """Given a vector in this coordinate system, return its expression in the basic CS.
        Any iterable containing floats of the same dimension as this CS will do for vector"""
        # Also, there probably is a better way of writing this...
        return np.array(self.origin.pos + np.dot(self.Q, vector))[0]


class Cord2R(CoordinateSystem):
    """A rectangular coordinate system entity, with its orientation given by the coordinates of
    three points with all the properties given to a Nastran CORD2R card"""
    def __init__(self, cid, rid, A1, A2, A3, B1, B2, B3, C1, C2, C3,
                 notation=None, construct=True, parent=None):
        self.cid = cid
        self.rid = rid
        self.A = Point((parsing.nas_float(A1), parsing.nas_float(A2), parsing.nas_float(A3)))
        self.B = Point((parsing.nas_float(B1), parsing.nas_float(B2), parsing.nas_float(B3)))
        self.C = Point((parsing.nas_float(C1), parsing.nas_float(C2), parsing.nas_float(C3)))
        self.notation = notation
        self.parent = parent
        self.type = 'rectangular'
        self.constructed = False
        if construct:
            self.construct()

    def construct(self):
        """This is the object's constructor method. It'll be run after the whole model has been
        read.
        This allows us to declare element cards before the grid point cards they're based on and so
        on."""
        if self.constructed:
            return

        # If the coordinates are expressed in a system other than the basic, construct
        # that CS and then express convert them
        if self.rid != 0:
            self.parent.coords[self.rid].construct()

        # Get the position of the origin of the CS in the basic CS
        if self.rid == 0:
            A = self.A.pos
            B = self.B.pos
            C = self.C.pos
        else:
            A = self.parent.coords[self.rid].toBasic(self.A.pos)
            B = self.parent.coords[self.rid].toBasic(self.B.pos)
            C = self.parent.coords[self.rid].toBasic(self.C.pos)

        # A small number, used to detect if points are colinear
        delta = min(np.linalg.norm(A)/1.E-6, 1.E-6)

        # The base of the new CS
        k = (B - A)
        if np.linalg.norm(k) < delta:
            raise ValueError('Points A & B in CS {} are colinear'.format(self.cid))
        j = np.cross(k, (C-A))
        if np.linalg.norm(j) < delta:
            raise ValueError('Points A & C in CS {} are colinear'.format(self.cid))
        i = np.cross(j, k)
        if np.linalg.norm(i) < delta:
            raise ValueError("Points A, B & C in CS {} don't form a base".format(self.cid))

        # Normalize the base
        i /= np.linalg.norm(i)
        j /= np.linalg.norm(j)
        k /= np.linalg.norm(k)

        # The base transformation matrix
        Q = np.matrix( (i, j, k) )

        super().__init__(A, Q)
        self.constructed = True

    def __repr__(self):
        if self.cid is not None:
            return '{}: ({})'.format(self.cid, ', '.join([str(pos) for pos in self.origin.pos]))
        else:
            return '({})'.format(', '.join([str(pos) for pos in self.origin.pos]))


def Cord2RParser(parent, notation, fields):
    """This function gets an iterable with the parameters in a Nastran CORD2R card and
    returns a Cord2R object"""
    # If needed, create the parent's dictionary
    parent.register_component('coords')

    # We want the parsing to fail if no node ID is given
    cid = int(fields[1])
    rid = int(parsing.value_or_default(fields, 2, 0))
    A1  = parsing.nas_float(parsing.value_or_default(fields, 3, 0.))
    A2  = parsing.nas_float(parsing.value_or_default(fields, 4, 0.))
    A3  = parsing.nas_float(parsing.value_or_default(fields, 5, 0.))
    B1  = parsing.nas_float(parsing.value_or_default(fields, 6, 0.))
    B2  = parsing.nas_float(parsing.value_or_default(fields, 7, 0.))
    B3  = parsing.nas_float(parsing.value_or_default(fields, 8, 0.))
    C1  = parsing.nas_float(parsing.value_or_default(fields, 9, 0.))
    C2  = parsing.nas_float(parsing.value_or_default(fields, 10, 0.))
    C3  = parsing.nas_float(parsing.value_or_default(fields, 11, 0.))
    parent.coords[cid] = Cord2R(cid, rid, A1, A2, A3, B1, B2, B3, C1, C2, C3,
                                notation=notation, construct=False, parent=parent)
