Nastran Model Reader
====================

This project consists of a few Python routines that can be used to
programmatically read some parts from 
[MSC Nastran](http://www.mscsoftware.com/product/msc-nastran) models.

The code was originally written for my [Master's thesis](https://gitlab.com/josebagar/PFC)
in aerospace engineering and only supports the few cards I needed at the time.
It is being published with the hope that it might somehow be useful to others but
it is by no means guaranteed to work for you.

Features
--------
Short, long and free notation as well as `INCLUDE` sentences are supported (they worked for me; more
extensive testing might be needed).

Supported Nastran cards are:

  * `CORD2R`
  * `GRID`
  * `CTRIA3`
  * `CQUAD4`
  * `DMI`
  
Unsupported cards are safely ignored but incoherences (missing but 
referenced `COORD` cards, for example) in the model will break 
ungracefully.

Extending the code
------------------
The code can be seen as a template where you can add more card parsers.
In order to extend the code, you should write a parser which takes
a list of inputs (an ordered list with the contents of the card;
you should not have to worry about the notation) and parses them
to construct an object.

If the object needs initializing, you should write the parsing code
in your `__init__` method and the initialization code in a method named
`construct`. Apart from that, you must register the card in
the `parsers` object in [Model.py](model.py).

Improvements
------------
Don't expect any. I might be able to help with small stuff if you need
to get this working, though.

Drop me a line to joseba.gar@gmail.com if you find the code useful!