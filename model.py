# Copyright 2014-2017 Joseba Echevarria García <joseba.gar@gmail.com>
#
# Licensed under the Apache License, Version 2.0 (the "License");
# you may not use this file except in compliance with the License.
# You may obtain a copy of the License at
#
#     http://www.apache.org/licenses/LICENSE-2.0
#
# Unless required by applicable law or agreed to in writing, software
# distributed under the License is distributed on an "AS IS" BASIS,
# WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
# See the License for the specific language governing permissions and
# limitations under the License.

import os

from misc import log
from elems.coords import Cord2RParser
from elems.point import GridParser
from elems.tria import CTria3Parser
from elems.quad import CQuad4Parser
from math.matrix import DmiParser
import parsing.parsing as parsing


class Model:
    """One day, Simba, everything the light touches will be yours"""
    def __init__(self, path, parse=True):
        """Create a model object for later use."""

        self.f = open(path, 'r')
        self.f.close()

        # Bounding box definition
        self.min = None
        self.max = None

        # The list of the components of a model. This includes elements, grid points, MPCs...
        # The list is initially empty and the different parsers will add the things they need
        self.components = []

        # Store the path for later use
        self.path = path

        # The list of registered parsers, will probably want to make this modular so that parsers
        # are loaded automatically at class loading, or at least add a plugin subsystem
        self.parsers = {
            'CORD2R': Cord2RParser,
            'GRID': GridParser,
            'CTRIA3': CTria3Parser, 'CQUAD4': CQuad4Parser,
            'DMI': DmiParser}

        # Parse the document, if asked to
        if parse:
            self.parse()

    def parse(self, path=None, generate_objects=True):
        """Actually read the BDF file, see what we understand.
            * path is the path to the nastran model file, if none given, the one used while
                   constructing the Model object will be used.
            * generate_objects: Whether to construct the element objects after reading the
                file or not."""

        if not path:
            path = self.path

        # Nastran include paths are relative to the main file, for whatever reason
        olddir = os.getcwd()
        currentdir = os.path.dirname(path)
        if currentdir != '':
            os.chdir(currentdir)

        # TODO: Perform more parsing tests, this'll probably blow up in a variety of cases...
        with open(os.path.basename(path), 'r') as f:
            card = None
            fields = []
            for line in f:
                # Don't really like that ".strip()"...
                line = line.strip().upper()

                # Handle includes separately
                if line.upper().startswith('INCLUDE'):
                    # I have no idea if Nastran prefers "'" or '"' for text, so support both
                    inc = line.replace('"', "'").split("'")[1]
                    self.parse(inc, False)

                # Parse known cards
                else:
                    blocks, notation = parsing.get_fields(line)
                    # If the card can be found in the list of known cards, parse the old card
                    # (if any) and start a new one
                    # Skip field 10(A)
                    if blocks[0] in self.parsers.keys():
                        if card:
                            self.parsers[fields[0]](self, cardnotation, fields)

                        card = blocks[0]
                        cardnotation = notation
                        fields = blocks
                    # Empty field? -> Must be a continuation card...
                    # Large field continuation cards include a single '*', but getFields
                    # should filter that. Also, skip field 1(B) and 10(B)
                    #
                    # This is actually not correct, since it's a continuation line
                    # as follows is valid:
                    # *GRID10 1.0
                    # But we'll ignore that, for the time being
                    # Also, I should really be looking if the first block is the same as last line's
                    # block[10], since that means that I'm looking at a continuation card
                    elif (blocks[0] == '' or blocks[0].startswith('+')) and card:
                        fields.extend(blocks[1:])
                    else:
                        # Parse the last card, if any
                        if card and fields[0] in self.parsers.keys():
                            self.parsers[fields[0]](self, cardnotation, fields)
                        # Unknown card type or not a card
                        card = None

            # If we didn't do this, we'd miss the card defined last
            if card:
                self.parsers[card](self, cardnotation, fields)

        # If told to do so, try to generate the objects from the stored info
        # This allows delayed creation of objects
        # Not the most intuitive code in the world, but allows for great flexibility
        if generate_objects:
            for component in self.components:
                if hasattr(self, component):
                    log.log('parsing {}...'.format(component))
                    for entity in getattr(self, component).keys():
                        if (hasattr(getattr(self, component)[entity], 'construct') and
                                callable(getattr(self, component)[entity].construct)):
                            getattr(self, component)[entity].construct()

        os.chdir(olddir)

    def register_component(self, component):
        """Register a new model component means adding it to the components list and creating
        an empty dictionary with its name in the model so that it can be accessed later.
        Registered components that implement a construct() method get construct()ed after
        the whole model has been read"""
        if not hasattr(self, component):
            setattr(self, component, {})

        if component not in self.components:
            self.components.append(component)

    def translate(self, displacement):
        """Move the model by adding the given displacement to the position of all its grids"""
        for gid in self.nodes:
            self.nodes[gid].pos += displacement

        self.min.pos += displacement
        self.max.pos += displacement

    def scale(self, factor):
        """Scale the model by the given factor"""
        for gid in self.nodes:
            self.nodes[gid].pos *= factor

        self.min.pos *= factor
        self.max.pos *= factor
